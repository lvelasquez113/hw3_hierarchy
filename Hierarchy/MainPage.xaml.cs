﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace Hierarchy
{
    // Learn more about making custom code visible in the Xamarin.Forms previewer
    // by visiting https://aka.ms/xamarinforms-previewer
    [DesignTimeVisible(false)]
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
        }
        async void button_1_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page1()); //goes to content page 1
        }
        async void button_new_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page2()); //goes to content page 1
        }

        async void Button_3_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page3()); //goes to content page 1
        }

        async void mainpage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200); //gives a little delay
            button_1.TextColor = Color.Crimson; //chnages button color to crimson
            button_new.TextColor = Color.Crimson; //changes button color to crimson
            Button_3.TextColor = Color.Crimson; //changes button color to crimson
        }

        private void mainpage_Disappearing(object sender, EventArgs e)
        {
            button_1.TextColor = Color.GhostWhite; //changes button color to Ghostwhite
        button_new.TextColor = Color.GhostWhite; //changes button color to Ghostwhite
            Button_3.TextColor = Color.GhostWhite; //changes button color to Ghostwhite
        }
    }
}


﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page3 : ContentPage
    {
        public Page3()
        {
            InitializeComponent();
        }

        async void newCat_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new Page4()); //goes to page 4
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await base.DisplayAlert("final page", "welcome to the last page", "ok"); //display a message to user
            ContentPage content = page3; //allows to manipulate the content page
            await Task.Delay(200); //little delay to see change
            page3.BackgroundColor = Color.Crimson; // changes the bachground color to crimson
        }

        async void ContentPage_Disappearing(object sender, EventArgs e)
        {
            ContentPage content = page3; //allows the content page to be manipulated
            await Task.Delay(200); //gives little delay to see results
            page3.BackgroundColor = Color.GhostWhite; //changes the backgroundcolor to white
        }
    }
}
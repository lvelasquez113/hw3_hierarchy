﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page2 : ContentPage
    {
        public Page2()
        {
            InitializeComponent();
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200); //gives little delay to see results
            cat1.Source = "cat1.jpg"; //changes the source of the picture displayed
            catext.TextColor = Color.Red; //changes the text color to red
        }

        async void ContentPage_Disappearing(object sender, EventArgs e)
        {
            await Task.Delay(200); //gives little delay to see results
            cat1.Source = "cat.jpg"; //changes the source of the picture to original one
            catext.TextColor = Color.Black; //changes background color to black
        }
    }
}
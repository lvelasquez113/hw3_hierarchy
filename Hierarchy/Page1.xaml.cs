﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Hierarchy
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Page1 : ContentPage
    {
        public Page1()
        {
            InitializeComponent();
        }

        async void ContentPage_Appearing(object sender, EventArgs e)
        {
            await Task.Delay(200); //gives a little delay to see change
            ContentPage content = page1; //allows the content page to be manipulated
            page1.BackgroundColor = Color.DarkBlue; //changes background color to dark blue

        }

        private void page1_Disappearing(object sender, EventArgs e)
        {
            //await Task.Delay(200);
            base.DisplayAlert("Dog Page", "Leaving the dog page", "ok"); //display message to user
            ContentPage content = page1; ////allows the content page to be manipulated
            page1.BackgroundColor = Color.LemonChiffon; //changes background color to lemonchiffon
        }
        //referance to the delay
        //https://stackoverflow.com/questions/25176528/trigger-an-action-to-start-after-x-milliseconds
    }
}